http = require 'http'
httpProxy = require 'http-proxy'

options = 
  router: 
    'localhost/search': 'localhost:8081/search'
    'localhost/': 'localhost:3000'

httpProxy.createServer( options ).listen( 9001 )

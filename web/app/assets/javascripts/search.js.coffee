# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  new Searcher()

class Searcher
  constructor: () ->
    @startListeners()
    _this = @

  startListeners: () ->
    @startHashChangeListener() 
    @startSearchFormListener()
    @startShowAuthorListeners()
    @startHideAuthorListeners()
    @startRecommendationListeners()
    @search()

  startHashChangeListener: () ->
    self = @
    $(window).hashchange () ->
      self.search()

  search: () ->
    params = window.location.hash.replace(/[\#\?]/g, '').split('&')
    hash = {}
    for param in params
      split = param.split '='
      hash[split[0]] = split[1]
    return unless hash.q?
    if hash.q.length == 0
      $('#searchResults').html ''
      $('#searchPagination').html ''
      return
    @doSearch( decodeURIComponent( hash.q ), hash.p )

  startSearchFormListener: () ->
    $('#searchForm').submit () ->
      query = $(this).find('input').val()
      window.location.hash = "?q=#{encodeURIComponent query}&p=#{1}"
      return false

  doSearch: ( query, page ) ->
    page = 1 unless page
    # This didn't work for me, I used "#{window.location.href}".replace(/#.*$/,"search") instead...
    # The getJSON was looking for localhost:9001/#?q=...&p=.../search, instead of just /search...
    # Not sure why but I have switched it back for now ~Scott
    #$.getJSON "#{window.location.href}".replace(/#.*$/,"search"), query: query, page: page, ( data ) ->
    $.getJSON "#{window.location.href}/search", query: query, page: page, ( data ) ->
      if data.error
        alert "Something horrible happened: \n\n#{data.error}"
      else
        hashbase = "?q=#{query}"
        if data.results.length == 0
          $('#searchResults').html '<li>No results found...</li>'
          $('#searchPagination').html ''
        else
          for result in data.results
            if result.authors.length > 2
              result.moreAuthors = result.authors.splice 2, result.authors.length - 2
              result.hasMoreAuthors = true
          $('#searchResults').html ich.search_results( data, true ) 
          prevpage = Math.max data.page - 1, 1
          nextpage = Math.min data.page + 1, data.pages
          pagedata =
            hashbase: hashbase
            prevpage: prevpage
            prevenabled: prevpage != data.page
            nextpage: nextpage
            nextenabled: nextpage != data.page
            pages: []

          range = []
          unless data.page - 5 > 0
            range = (i for i in [1..Math.min(data.pages, 10)])
          else unless data.page + 4 > data.pages
            range = (i for i in [data.page-5..data.page+4])
          else
            range = (i for i in [Math.max(1, data.pages-10)..data.pages])

          for i in range
            pagedata.pages[i] =
              idx: i
              active: i == data.page

          $('#searchPagination').html ich.pagination( pagedata, true )
          document.body.scrollTop = document.documentElement.scrollTop = 0
    false

  doRecommend: ( pmid ) ->
    page = 1
    # Same comment as in doSearch regarding this getJSON attempt. I had to use
    # "#{window.location.href}".replace(/#.*$/,"search") instead, but switched
    # it to mimic the one in doSearch since apparently that works for the
    # rest of you. ~Scott
    #$.getJSON "#{window.location.href}".replace(/#.*$/,"search"), recommend: pmid, ( data ) ->
    $.getJSON "#{window.location.href}/search", recommend: pmid, ( data ) ->
      if data.error
        alert "Something horrible happened: \n\n#{data.error}"
      else
        if data.results.length == 0
          $('#recommendResults').html '<li>No related articles...</li>'
        else
          for result in data.results
            if result.authors.length > 2
              result.moreAuthors = result.authors.splice 2, result.authors.length - 2
              result.hasMoreAuthors = true
          $('#recommendResults').html ich.recommend_results( data, true )
    false


  startShowAuthorListeners: () ->
    $('#searchResults').on 'click', 'a.showAuthors', () ->
      $(this).parent().parent().find('li.moreAuthors, li.hideAuthors').removeClass 'hidden'
      $(this).parent().parent().find('li.showAuthors').addClass 'hidden'
      false

  startHideAuthorListeners: () ->
    $('#searchResults').on 'click', 'a.hideAuthors', () ->
      $(this).parent().parent().find('li.moreAuthors, li.hideAuthors').addClass 'hidden'
      $(this).parent().parent().find('li.showAuthors').removeClass 'hidden'
      false

  startRecommendationListeners: () ->
    self = @
    $('#searchResults').on 'click', 'a.recommend', () ->
      pmid = this.id
      self.doRecommend(pmid)
     
      
      


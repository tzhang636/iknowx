package edu.illinois.cs.timan.iknowx.index;

import java.util.ArrayList;

import com.aliasi.medline.SearchableMedlineCodec;


/**
 * 
 * @author Sean Massung
 *
 */
public class SearchQuery
{
    private ArrayList<String> fields;
    private String queryText;
    private int numResults;
    private int fromYear;
    private int toYear;
    private int from;
    
    private final static int defaultNumResults = 25;
    private final static String defaultMedlineField = SearchableMedlineCodec.ABSTRACT_FIELD;
    
    public SearchQuery queryText(String queryText)
    {
        this.queryText = queryText;
        return this;
    }
    
    public SearchQuery fields(ArrayList<String> fields)
    {
        this.fields = new ArrayList<String>(fields);
        return this;
    }
    
    public ArrayList<String> fields()
    {
        return fields;
    }
    
    public String queryText()
    {
        return queryText;
    }
    
    public SearchQuery fields(String field)
    {
        fields = new ArrayList<String>();
        fields.add(field);
        return this;
    }
    
    public int numResults()
    {
        return numResults;
    }

    public SearchQuery numResults(int numResults)
    {
        this.numResults = numResults;
        return this;
    }

    public int fromYear()
    {
        return fromYear;
    }

    public SearchQuery fromYear(int fromYear)
    {
        this.fromYear = fromYear;
        return this;
    }

    public int toYear()
    {
        return toYear;
    }

    public SearchQuery toYear(int toYear)
    {
        this.toYear = toYear;
        return this;
    }

    public int fromDoc()
    {
        return from;
    }

    public SearchQuery fromDoc(int fromDoc)
    {
        this.from = fromDoc;
        return this;
    }
    
    /**
     * Constructor.
     * @param queryText
     * @param numResults
     * @param fields
     */
    public SearchQuery(String queryText, ArrayList<String> fields)
    {
        this.queryText = queryText;
        this.numResults = defaultNumResults;
        this.fields = fields;
        fromYear = 0;
        toYear = Integer.MAX_VALUE;
        from = 0;
    }
    
    public SearchQuery()
    {
        this.queryText = null;
        this.numResults = defaultNumResults;
        this.fields = new ArrayList<String>();
        fields.add(defaultMedlineField);
        fromYear = 0;
        toYear = Integer.MAX_VALUE;
        from = 0;
    }
    
    /**
     * Determines if the range query functionality is enabled.
     * @return if this query is a range query
     */
    public boolean isRangeQuery()
    {
        return fromYear != 0 && toYear != Integer.MAX_VALUE;
    }
    
    /**
     * Constructor.
     * @param queryText
     * @param numResults
     * @param field
     */
    public SearchQuery(String queryText, String field)
    {
        this.queryText = queryText;
        this.numResults = defaultNumResults;
        fields = new ArrayList<String>();
        fields.add(field);
        fromYear = 0;
        toYear = Integer.MAX_VALUE;
    }
    
    /**
     * Tells whether two objects are both SearchQueries with equal contents.
     * @param other
     * @return true if the objects are equal
     */
    public boolean equals(Object other)
    {
        if(!(other instanceof SearchQuery))
            return false;
        
        SearchQuery otherQuery = (SearchQuery) other;
        return otherQuery.queryText.equals(queryText) &&
                otherQuery.fields == fields &&
                otherQuery.numResults == numResults &&
                otherQuery.fromYear == fromYear &&
                otherQuery.toYear == toYear &&
                otherQuery.from == from;
    }
}

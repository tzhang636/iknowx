package edu.illinois.cs.timan.iknowx.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.aliasi.medline.parser.MedlineCitation;


/**
 * 
 * @author Sean Massung
 *
 */
public class SearchResult
{
    private ArrayList<MedlineCitation> results;
    private int totalHits;
    private SearchQuery searchQuery;
    private HashMap<String, String> htmlSnippets; // map pmid to highlighted string
    
    /**
     * Default constructor to represent an empty search result.
     */
    public SearchResult(SearchQuery searchQuery)
    {
        totalHits = 0;
        results = new ArrayList<MedlineCitation>();
        this.searchQuery = searchQuery;
        htmlSnippets = new HashMap<String, String>();
    }

    /**
     * 
     * @param results
     * @param totalHits
     */
    public SearchResult(SearchQuery searchQuery, int totalHits)
    {
        this.results = new ArrayList<MedlineCitation>();
        this.totalHits = totalHits;
        this.searchQuery = searchQuery;
        htmlSnippets = new HashMap<String, String>();
    }
    
    public void addResult(MedlineCitation cit)
    {
        results.add(cit);
    }
    
    /**
     * Set the highlighted text for this document.
     * @param cit
     * @param snippet
     */
    public void setSnippet(MedlineCitation cit, String snippet)
    {
        htmlSnippets.put(cit.pmid(), snippet);
    }
    
    /**
     * 
     * @param cit
     * @return
     */
    public String getSnippet(MedlineCitation cit)
    {
        return htmlSnippets.get(cit.pmid());
    }
    
    /**
     * @return the query that returned this result
     */
    public SearchQuery query()
    {
        return searchQuery;
    }
    
    /**
     * @return an ArrayList of medline articles matching the query
     */
    public ArrayList<MedlineCitation> getDocs()
    {
        return results;
    }
    
    /**
     * @return the total number of hits from the query
     */
    public int numHits()
    {
        return totalHits;
    }

    /**
     * Temporarily used to create paginated results.
     * @param from
     * @param to
     */
    public void trimResults(int from)
    {
        // bounds checking
        if( from >= results.size() ) {
            results = new ArrayList<MedlineCitation>();
            return;
        }

        int to = results.size();

        // trimming
        List<MedlineCitation> newResults = results.subList(from, to);
        results = new ArrayList<MedlineCitation>(newResults);
    }

    /**
     * Tells whether two objects are both SearchQueries with equal contents.
     * @param other
     * @return true if the objects are equal
     */
    public boolean equals(Object other)
    {
        if(!(other instanceof SearchResult))
            return false;
        
        SearchResult otherResult = (SearchResult) other;
        return otherResult.searchQuery.equals(searchQuery) &&
                otherResult.results == results &&
                otherResult.totalHits == totalHits;
                
    }
}

package edu.illinois.cs.timan.iknowx.index;

import java.util.Scanner;

import com.aliasi.medline.SearchableMedlineCodec;
import com.aliasi.medline.parser.Article;
import com.aliasi.medline.parser.JournalIssue;
import com.aliasi.medline.parser.MedlineCitation;


/**
 * Tests out the recommendation process.
 * @author Scott Abbey
 */
public class RecommendTester
{
    /**
     * Perform a search, choose an article, then show recommendations from it.
     * Basically copied from SearchTester.
     *
     * @param indexLocation
     */
    static void search(Scanner scan, Searcher searcher)
    {
    	System.out.println("Enter search term: ");
        String queryText = scan.nextLine();
        
        SearchQuery searchQuery = new SearchQuery(queryText, SearchableMedlineCodec.ABSTRACT_FIELD);
        SearchResult result = searcher.search(searchQuery);
        int resultNum = 0;
        for(MedlineCitation cit: result.getDocs())
        {
            Article article = cit.article();
            System.out.println(++resultNum + ". " + article.articleTitle());
            JournalIssue issue = article.journal().journalIssue();
            System.out.println("\t" + issue.pubDate().month() + " " + issue.pubDate().year());
            System.out.println("\t" + cit.pmid());
        }
        
        
        System.out.println("Which number to recommend from?");
        String input = scan.nextLine();
        int i = Integer.parseInt(input);
        SearchResult theseRecs = searcher.recommend(result.getDocs().get(i-1).pmid());
        
        resultNum = 0;
        for(MedlineCitation cit: theseRecs.getDocs())
        {
            Article article = cit.article();
            System.out.println(++resultNum + ". " + article.articleTitle());
            JournalIssue issue = article.journal().journalIssue();
            System.out.println("\t" + issue.pubDate().month() + " " + issue.pubDate().year());
            System.out.println("\t" + cit.pmid());
            System.out.println(article.abstrct());
        }
        
    }

    /**
     * Indexes and searches.
     * @param args
     */
    public static void main(String[] args)
    {
    	// Same location from SearchTester...
        String indexLocation = "lucene-small-2012";
        Scanner scan = new Scanner(System.in);
        Searcher searcher = new Searcher(indexLocation);
        
        while(true)
        {
        	search(scan, searcher);
        }
    }

}

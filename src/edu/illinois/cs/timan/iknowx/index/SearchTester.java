package edu.illinois.cs.timan.iknowx.index;

import com.aliasi.medline.SearchableMedlineCodec;
import com.aliasi.medline.parser.Article;
import com.aliasi.medline.parser.JournalIssue;
import com.aliasi.medline.parser.MedlineCitation;


/**
 * Simply runs the Searcher and Indexer classes for debugging or tutorial purposes.
 * @author Sean Massung
 */
public class SearchTester
{
    /**
     * Show how to use the Searcher class.
     * @param indexLocation
     */
    static void search(String indexLocation)
    {
        Searcher searcher = new Searcher(indexLocation);
        String queryText = "cancer";
        
        SearchQuery searchQuery = new SearchQuery(queryText, SearchableMedlineCodec.ABSTRACT_FIELD);
        SearchResult result = searcher.search(searchQuery);
        int resultNum = 0;
        for(MedlineCitation cit: result.getDocs())
        {
            Article article = cit.article();
            System.out.println(++resultNum + ". " + article.articleTitle());
            JournalIssue issue = article.journal().journalIssue();
            System.out.println("\t" + issue.pubDate().month() + " " + issue.pubDate().year());
            System.out.println(result.getSnippet(cit));
        }
    }

    /**
     * Indexes and searches.
     * @param args
     */
    public static void main(String[] args)
    {
        String indexLocation = "lucene-small-2012";
        String indexFiles = "data/small-2012";
        if(!Indexer.index(indexLocation, indexFiles))
        {
            System.out.println("Error indexing!");
            return;
        }
        search(indexLocation);
    }

}

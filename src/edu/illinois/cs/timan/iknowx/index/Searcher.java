package edu.illinois.cs.timan.iknowx.index;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

import com.aliasi.medline.MedlineCodec;
import com.aliasi.medline.SearchableMedlineCodec;
import com.aliasi.medline.parser.MedlineCitation;

/**
 * 
 * @author Sean Massung
 * 
 */
public class Searcher
{
    private IndexSearcher indexSearcher;
    private Analyzer analyzer;
	private MoreLikeThis moreLikeThis;
    private static SimpleHTMLFormatter formatter;
    private static final int numFragments = 4;

    private final static String defaultMedlineField = SearchableMedlineCodec.ABSTRACT_FIELD;

    /**
     * Sets up the lucene index Searcher with the specified index.
     * 
     * @param indexPath
     *            The path to the desired lucene index.
     */
    public Searcher(String indexPath)
    {
        try
        {
            IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(indexPath)));
            indexSearcher = new IndexSearcher(reader);
            analyzer = new StandardAnalyzer(Version.LUCENE_40);
            formatter = new SimpleHTMLFormatter("<strong>", "</strong>");
            
            
            // Set up moreLikeThis
			moreLikeThis = new MoreLikeThis(reader);
			moreLikeThis.setAnalyzer(analyzer);
			
			// Just searching the abstract -- for now.
			moreLikeThis.setFieldNames(new String[]{SearchableMedlineCodec.ABSTRACT_FIELD});
			
			// There are some other options available.
			moreLikeThis.setMinTermFreq(1);
			moreLikeThis.setMinDocFreq(1);
            
        }
        catch(IOException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * The main search function.
     * @param searchQuery Set this object's attributes as needed.
     * @return
     */
    public SearchResult search(SearchQuery searchQuery)
    {
        BooleanQuery combinedQuery = new BooleanQuery();

        // add range query info if necessary
        if(searchQuery.isRangeQuery())
        {
            BytesRef fromBytes = new BytesRef(String.valueOf(searchQuery.fromYear()));
            BytesRef toBytes = new BytesRef(String.valueOf(searchQuery.toYear()));
            TermRangeQuery rangeQuery = new TermRangeQuery(SearchableMedlineCodec.DATE_YEAR_FIELD,
                    fromBytes, toBytes, true, true);
            combinedQuery.add(rangeQuery, BooleanClause.Occur.MUST);
        }

        // add each field
        for(String field: searchQuery.fields())
        {
            field = checkField(field);
            QueryParser parser = new QueryParser(Version.LUCENE_40, field, analyzer);
            try
            {
                Query textQuery = parser.parse(searchQuery.queryText());
                combinedQuery.add(textQuery, BooleanClause.Occur.MUST);
            }
            catch(ParseException exception)
            {
                exception.printStackTrace();
            }
        }
        
        return runSearch(combinedQuery, searchQuery);
    }

    /**
     * The simplest search function. Searches the abstract field and returns a
     * the default number of results.
     * 
     * @param queryText
     *            The text to search
     * @return the SearchResult
     */
    public SearchResult search(String queryText)
    {
        return search(new SearchQuery(queryText, defaultMedlineField));
    }
    
    
    
    
	/**
	 * Find documents similar to a given document.
	 * @param pmid the pubmed id of the document to recommend from
	 * @return
	 */
	public SearchResult recommend(String pmid)
	{

		Query moreLikeThisQuery = null;
		String title = "";
		try {

			Term pmidTerm = new Term(SearchableMedlineCodec.PUBMED_ID_FIELD, pmid);
			TermQuery termQuery = new TermQuery(pmidTerm);

	        //BooleanQuery booleanQuery = new BooleanQuery();
	        //booleanQuery.add(termQuery, BooleanClause.Occur.MUST);

            TopDocs docs = indexSearcher.search(termQuery, 10);
			ScoreDoc[] hits = docs.scoreDocs;
            Integer recommenderLuceneID = 0;
            if( docs.totalHits >= 1) {
            	recommenderLuceneID = hits[0].doc;
                Document doc = indexSearcher.doc(hits[0].doc);
				MedlineCodec codec = new MedlineCodec();
				MedlineCitation cit = codec.toObject(doc);
            	title = cit.article().articleTitle();
			}

			moreLikeThisQuery = moreLikeThis.like( recommenderLuceneID );

		}
		catch(IOException exception)
		{
			exception.printStackTrace();
		}

		// For highlighting, just pick words from the title for now
		SearchQuery searchQuery = new SearchQuery(title, defaultMedlineField);
		
		// Top 10 should suffice
		searchQuery.numResults(10);
		// And the first result should be the one we were recommending from, throw it away
		searchQuery.fromDoc(1);
		
		return runSearch(moreLikeThisQuery, searchQuery);
	}

    
    
    
    

    /**
     * Performs the actual lucene search.
     * 
     * @param luceneQuery
     * @param numResults
     * @return the SearchResult
     */
    private SearchResult runSearch(Query luceneQuery, SearchQuery searchQuery)
    {
        try
        {
            indexSearcher.setSimilarity(new BM25Similarity());
            TopDocs docs = indexSearcher.search(luceneQuery, searchQuery.fromDoc() + searchQuery.numResults());
            ScoreDoc[] hits = docs.scoreDocs;
            String field = searchQuery.fields().get(0);
            
            SearchResult searchResult = new SearchResult(searchQuery, docs.totalHits);
            for(ScoreDoc hit : hits)
            {
                Document doc = indexSearcher.doc(hit.doc);
                MedlineCodec codec = new MedlineCodec();
                MedlineCitation cit = codec.toObject(doc);
                
                String highlighted = null;
                try
                {
                    Highlighter highlighter = new Highlighter(formatter, new QueryScorer(luceneQuery));
                    String contents = doc.getField(field).stringValue();
                    String[] snippets = highlighter.getBestFragments(analyzer, field, contents, numFragments);
                    highlighted = createOneSnippet(snippets);
                }
                catch(InvalidTokenOffsetsException exception)
                {
                    exception.printStackTrace();
                    highlighted = "(no snippets yet)";
                }

                searchResult.addResult(cit);
                searchResult.setSnippet(cit, highlighted);
            }
            
            searchResult.trimResults(searchQuery.fromDoc());
            return searchResult;
        }
        catch(IOException exception)
        {
            exception.printStackTrace();
        }
        return new SearchResult(searchQuery);
    }

    /**
     * Create one string of all the extracted snippets from the highlighter
     * @param snippets
     * @return
     */
    private String createOneSnippet(String[] snippets)
    {
        String result = "... ";
        for(String s: snippets)
            result += s + " ...";
        return result;
    }

    /**
     * @param fieldName
     * @return the default medline field if the parameter is not a valid field
     *         name
     */
    private String checkField(String fieldName)
    {
        if(!SearchableMedlineCodec.isValidField(fieldName))
        {
            System.out.println("Warning: invalid field specified. Changing to default field.");
            return defaultMedlineField;
        }
        return fieldName;
    }
}

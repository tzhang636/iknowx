package edu.illinois.cs.timan.iknowx.index;

import org.apache.lucene.search.similarities.BasicStats;
import org.apache.lucene.search.similarities.SimilarityBase;

/**
 * 
 * @author Sean Massung
 *
 */
public class IknowSimilarity extends SimilarityBase
{
    @Override
    protected float score(BasicStats stats, float arg1, float arg2)
    {
        return 0;
    }

    @Override
    public String toString()
    {
        return null;
    }

}

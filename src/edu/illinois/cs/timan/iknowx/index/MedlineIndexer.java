package edu.illinois.cs.timan.iknowx.index;

import java.io.IOException;
import java.util.HashSet;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;

import com.aliasi.medline.MedlineCodec;
import com.aliasi.medline.lucene.Fields;
import com.aliasi.medline.parser.MedlineCitation;
import com.aliasi.medline.parser.MedlineHandler;

class MedlineIndexer implements MedlineHandler
{
    final IndexWriter mIndexWriter;
    final MedlineCodec mMedlineCodec;
    final boolean sIsBaseline = true;
    private HashSet<String> seen;

    public MedlineIndexer(IndexWriter indexWriter, MedlineCodec codec, HashSet<String> seen) throws IOException
    {
        mIndexWriter = indexWriter;
        mMedlineCodec = codec;
        this.seen = seen;
    }

    public void handle(MedlineCitation citation)
    {
        Document doc = mMedlineCodec.toDocument(citation);
        try
        {
            if(sIsBaseline)
            {
                if(!seen.contains(citation.pmid()))
                {
                    mIndexWriter.addDocument(doc);
                    seen.add(citation.pmid());
                }
                else
                {
                    System.out.println("    duplicate article found (pmid = " + citation.pmid() + ")");
                }
            }
            else
            {
                if(!seen.contains(citation.pmid()))
                {
                    mIndexWriter.addDocument(doc);
                    Term idTerm = new Term(Fields.ID_FIELD, citation.pmid());
                    mIndexWriter.addDocument(doc);
                }
                else
                {
                    System.out.println("    duplicate article found (pmid = " + citation.pmid() + ")");
                }
            }
        } catch(IOException e)
        {
            System.out.println("handle citation: index access error, term: " + citation.pmid());
        }
    }

    public void delete(String pmid)
    {
        return;
    }

    public void close() throws IOException
    {
        mIndexWriter.commit();
    }
}
package edu.illinois.cs.timan.iknowx.index;


import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.aliasi.medline.SearchableMedlineCodec;
import com.aliasi.medline.parser.MedlineParser;
import com.aliasi.util.Strings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashSet;
import java.util.zip.GZIPInputStream;

/**
 * Indexes medline articles, based on IndexMedline.java from lingpipe project.
 * 
 * @author Sean Massung
 * @author Mitzi Morris
 */
public class Indexer
{
    private final static double defaultRAMBufferMBSize = 4096.0;

    /**
     * Creates a lucene index for the given documents.
     * 
     * @param indexPath
     *            Path to where the lucene index will be created
     * @param documentsPath
     *            Path to the documents to index
     * @return whether creating the index was successful
     */
    public static boolean index(String indexPath, String documentsPath)
    {
        final File docDir = new File(documentsPath);
        if(!docDir.exists() || !docDir.canRead())
        {
            System.out.println("Error reading " + docDir.getAbsolutePath());
            return false;
        }

        Date start = new Date();
        boolean create = true;
        if(!indexDocs(indexPath, documentsPath, create))
            return false;
        Date end = new Date();

        long seconds = (end.getTime() - start.getTime()) / 1000;
        if(seconds <= 60)
            System.out.println("Done!\n" + seconds + " seconds");
        else
            System.out.println("Done!\n" + (float) seconds / 60 + " minutes");
        return true;
    }

    /**
     * Calls the Medline parser on a file.
     * 
     * @param parser
     * @param file
     */
    private static void parseFile(MedlineParser parser, File file)
    {
        if(file.getName().endsWith("xml"))
        {
            try
            {
                InputSource inSource = new InputSource(file.getAbsolutePath());
                parser.parse(inSource);
            }
            catch(IOException exception)
            {
                exception.printStackTrace();
            }
        }
        else if(file.getName().endsWith("gz"))
        {
            try
            {
                parseGZip(parser, file);
            }
            catch(IOException exception)
            {
                exception.printStackTrace();
            }
            catch(SAXException exception)
            {
                exception.printStackTrace();
            }
        }
        else
        {
            String msg = "Unknown file extension for " + file.getName();
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Uncompresses a gzip file and hands it off to the parser.
     * 
     * @param parser
     * @param file
     * @throws IOException
     * @throws SAXException
     */
    private static void parseGZip(MedlineParser parser, File file) throws IOException, SAXException
    {
        FileInputStream fileIn = null;
        GZIPInputStream gzipIn = null;
        InputStreamReader inReader = null;
        BufferedReader bufReader = null;
        InputSource inSource = null;
        try
        {
            fileIn = new FileInputStream(file);
            gzipIn = new GZIPInputStream(fileIn);
            inReader = new InputStreamReader(gzipIn, Strings.UTF8);
            bufReader = new BufferedReader(inReader);
            inSource = new InputSource(bufReader);
            inSource.setSystemId(file.toURI().toURL().toString());
            parser.parse(inSource);
        }
        finally
        {
            bufReader.close();
            inReader.close();
            gzipIn.close();
            fileIn.close();
        }
    }

    /**
     * Private helper for Indexer.
     * 
     * @param indexPath
     *            The path to create the lucene index
     * @param documentsPath
     *            The path to files to index
     * @param create
     *            True if only creating files, false if creating or appending
     *            files
     * @return whether an index was successfully created
     */
    private static boolean indexDocs(String indexPath, String documentsPath, boolean create)
    {
        System.out.println("Indexing " + documentsPath + "...");

        Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);

        if(create)
        {
            config.setOpenMode(OpenMode.CREATE);
        }
        else
        {
            config.setOpenMode(OpenMode.CREATE_OR_APPEND);
        }

        config.setRAMBufferSizeMB(defaultRAMBufferMBSize);

        FSDirectory dir;
        IndexWriter writer;
        try
        {
            dir = FSDirectory.open(new File(indexPath));
            writer = new IndexWriter(dir, config);
        }
        catch(IOException exception1)
        {
            exception1.printStackTrace();
            return false;
        }

        SearchableMedlineCodec codec = new SearchableMedlineCodec();
        HashSet<String> seen = new HashSet<String>();
        int prevDocsAdded = 0;
        String[] files = new File(documentsPath).list();
        int numDone = 0;
        for(String filename : files)
        {
            System.out.println("  parsing " + filename + " (" + ++numDone + "/" + files.length + ")");
            boolean saveRawXML = true;
            MedlineParser parser = new MedlineParser(saveRawXML);
            try
            {
                MedlineIndexer indexer = new MedlineIndexer(writer, codec, seen);
                parser.setHandler(indexer);
                parseFile(parser, new File(documentsPath + "/" + filename));
                indexer.close();
            }
            catch(IOException exception)
            {
                exception.printStackTrace();
                return false;
            }
            System.out.println("    added " + (seen.size() - prevDocsAdded) + " documents (" + seen.size() + " total)");
            prevDocsAdded = seen.size();
        }

        try
        {
            writer.close();
        }
        catch(IOException exception)
        {
            System.out.println("Warning: could not close IndexWriter.");
            exception.printStackTrace();
        }

        return true;
    }
}
package edu.illinois.cs.timan.iknowx.api;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.util.ajax.JSON;

import com.aliasi.medline.parser.Author;
import com.aliasi.medline.parser.MedlineCitation;

import edu.illinois.cs.timan.iknowx.index.SearchQuery;
import edu.illinois.cs.timan.iknowx.index.SearchResult;
import edu.illinois.cs.timan.iknowx.index.Searcher;



public class SearchServlet extends HelloWorldServlet {
	private static final long serialVersionUID = -8020111169469835645L;

    private static final Searcher searcher = new Searcher( "/srv/data/IKNOWX/Data/lucene-all" );

    private static final int PAGE_SIZE = 25;

    public class APIException extends Exception {
		private static final long serialVersionUID = -3562991459585959513L;
    }

    @Override
    protected void doGet( HttpServletRequest request, 
            HttpServletResponse response ) throws ServletException, IOException {
        try {
            Map<String, String[]> params = request.getParameterMap();

			if( params.containsKey( "query" ) || params.containsKey( "recommend" ) )
			{
				SearchResult result = null;
            int page = 1;

				// Different search method for query vs. recommend
				if( params.containsKey( "query" ) ){

            if( params.containsKey( "page" ) )
                page = Integer.parseInt( params.get("page")[0] );

            SearchQuery query = new SearchQuery()
                .queryText( params.get("query")[0] )
                .fromDoc( PAGE_SIZE * ( page - 1 ) )
                .numResults( PAGE_SIZE );

					result = searcher.search( query );

				} else if( params.containsKey( "recommend" ) ) {

					result = searcher.recommend(params.get("recommend")[0]);

				}
				// Prepare the rest the same way for both
            ArrayList<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
            for( MedlineCitation cit : result.getDocs() ) {
                HashMap<String, Object> doc = new HashMap<String, Object>();
                doc.put( "title", cit.article().articleTitleText() );
                
                doc.put( "abstract", result.getSnippet( cit ) );

                ArrayList<String> authors = new ArrayList<String>();
                for( Author auth : cit.article().authorList().authors() ) {
                    if( auth.isCollective() )
                        authors.add( auth.collectiveName() );
                    else
                        authors.add( auth.name().fullName() );
                }
                doc.put( "authors", authors );

                doc.put( "pmid", cit.pmid() );

                results.add( doc );
            }

            Map<String, Object> responseMap = new HashMap<String, Object>();
            responseMap.put( "results", results );
            responseMap.put( "hits", result.numHits() );

            int totalPages = result.numHits() / PAGE_SIZE;
            if( result.numHits() % PAGE_SIZE > 0 )
                totalPages += 1;
            responseMap.put( "pages", totalPages );
            responseMap.put( "page", page );

            responseMap.put( "error", null );
            response.getWriter().println( JSON.toString( responseMap ) );


			} else {

				throw new APIException();
			}

        } catch( Exception ex ) {
            ex.printStackTrace();
            badRequest( response );
        }
    }

    void badRequest( HttpServletResponse response ) throws IOException {
        response.setContentType("application/json");
        Map<String, Object> badReqMap = new HashMap<String, Object>();
        badReqMap.put( "results", null );
        badReqMap.put( "error", "Bad request" );
        response.getWriter().println( JSON.toString( badReqMap ) );
    }
}

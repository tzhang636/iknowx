package edu.illinois.cs.timan.iknowx.api;

import java.io.IOException;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

class HelloWorldServlet extends HttpServlet {
	private static final long serialVersionUID = 4963236479153054344L;

    @Override
	protected void doGet( HttpServletRequest request, 
            HttpServletResponse response ) 
    throws ServletException, IOException {
        response.setContentType( "text/html;charset=utf-8" );
        response.setStatus( HttpServletResponse.SC_OK );
        response.getWriter().println("OHI");
    }
}

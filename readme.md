# Setting up the project

Set up mvn and generate an eclipse project

- mvn install
- mvn eclipse:eclipse

Set up Eclipse

- File -> Import -> Existing Project
- Window -> Preferences -> Classpath Variables -> New
- Name it M2_REPO, and give it the path to ~/.m2/repository

Install required gems

- cd web/
- bundle install

Initialize the database -- Rails complains if it's not there even if we're not
using it.

- cd web/
- mkdir db
- rake db:create

Set up nodejs

- cd web/node/
- npm install
- sudo npm -g install coffee-script

# Creating an index

- Run the SearchTester class and set the correct paths once you have the data
  files downloaded
- Make sure the index path in SearchTester and SearchServlet are the same

# Running the server locally

Start the rails server

- cd web/
- rails s

Start the jetty server

- mvn package
- java -jar target/IKNOW-X-0.1-SNAPSHOT.jar

Start the proxy

- cd web/node
- coffee proxy.coffee

Now you can navigate to http://localhost:9001/ and use IKNOW-Medical
